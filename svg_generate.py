#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import time
import shutil
import bpy
import re
import os
from pathlib import Path as Filepath
from prj.drawing_camera import get_drawing_camera
from prj.working_scene import get_working_scene
from prj.progress_indicator import get_progress_indicator
from prj.utils import UNITS_SYSTEM, get_drawing_styles
from prj.svg_utils import subjects_in_svg, get_abs_svg
from prj.svglib import AbsLayer
from prj.svgread import Svg_read

BASE_CSS = 'prj.css'
PRJ_DIR = Filepath(__file__).parent.absolute()
ASSETS_DIR = Filepath(PRJ_DIR / 'assets')
get_css_style = lambda location: f"@import url({location}{BASE_CSS});"
sorted_dict = lambda ds:sorted(ds, key=lambda x: ds[x].position)
re_stroke_width = re.compile(r'(stroke-width\s*:)\s*([\.\d]*)')
re_stroke_dasharray = re.compile(r'(stroke-dasharray\s*:)([\s\.\d]*)')

layers_labels = []

def get_svgs_from_svg_data(subjects: list['Drawing_subject']) -> None:
    """ Create svgs form svg_data """
    print('Start rewriting svg')
    from prj.svg_data import svgs_data
    global layers_labels
    rewrite_svgs_start_time = time.time()
    progress = get_progress_indicator()
    progress.set_phase(phase_data={'name': 'collect_subject_svgs',
                'start':progress.get_last_update(), 
                'end':progress.get_next_step('main'), 'steps':len(svgs_data) })

    cam = get_drawing_camera()
    working_scene = get_working_scene()
    drawing_styles = get_drawing_styles()
    linked = cam.linking_type == 'linked'
    scale_factor = UNITS_SYSTEM[working_scene.scene.unit_settings.system][
            'svg_factor'] * working_scene.resolution_correction
    layers_labels = [drawing_styles[d_style].name for d_style in \
            sorted_dict(drawing_styles)]
    if not linked:
        ## Create a brand new svg for all the subjects
        abs_svg = get_abs_svg(get_working_scene().svg_size, 
                get_css_style('./'), layers_labels)

    for i, svg_data in enumerate(svgs_data):
        progress.update(phase_name='collect_subject_svgs', step=i)
        if svg_data.subject not in subjects:
            continue
        if linked:
            ## Create a brand new svg for the subject
            abs_svg = get_abs_svg(get_working_scene().svg_size, 
                    get_css_style('../'), layers_labels)
        svg_data.data_to_svg(abs_svg, scale_factor)
        if linked and cam.export_svg:
            ## Make the abs_svg real and add subject data to it
            subj_svg = abs_svg.to_real(svg_data.path)
            with open(svg_data.path, "a") as svg_file:
                append_subject_data(svg_file, svg_data.subject)

    if not linked and cam.export_svg:
        ## Make the abs_svg real
        subj_svg = abs_svg.to_real(cam.drawing_filepath)
    print(f'\t...completed in {(time.time() - rewrite_svgs_start_time)}\n')

def append_subject_data(svg_file: 'io.TextIOWrapper', 
        subject: 'Drawing_subject') -> None:
    """ Append data about subject in svg_file """
    svg_file.write("<!--" + os.linesep)
    working_scene = get_working_scene()
    svg_file.write(f'{{')
    svg_file.write(f'"subject": "{subject.name}",')
    svg_file.write(os.linesep)
    svg_file.write(f'"original_name": "{subject.original_name}",')
    svg_file.write(os.linesep)
    svg_file.write(f'"resolution": {working_scene.get_resolution()},')
    for cond in subject.conditions:
        svg_file.write(os.linesep)
        svg_file.write(f'"{cond}": "{subject.conditions[cond]}",')
    svg_file.write(os.linesep)
    svg_file.write('"overlaps": [')
    for over_subj in subject.overlapping_subjects:
        lib = str(over_subj.lib_path) if over_subj.lib_path else None
        svg_file.write(f'("{over_subj.name}", "{lib}"),')
    svg_file.write('],')
    svg_file.write(os.linesep)
    ## pixel are collected in ranges with first and last included
    svg_file.write(f'"render_pixels": {subject.pixels_range},')
    svg_file.write(f'}}')
    svg_file.write(os.linesep)
    svg_file.write("-->")

def css_to_inches(css_src: Filepath, css_dest: Filepath) -> None:
    """ Convert millimeter values in css to inches values """
    with open(css_src, encoding='utf-8') as f:
        css_content = f.readlines()
    new_css = []
    for line in css_content:
        stroke_search = re_stroke_width.search(line)
        dasharray_search = re_stroke_dasharray.search(line)
        if not stroke_search and not dasharray_search:
            new_css.append(line)
            continue
        if stroke_search:
            new_stroke_width = float(stroke_search.group(2)) / 25.4
            new_line = re_stroke_width.sub(r'\1 ' + str(new_stroke_width), line)
        elif dasharray_search:
            dasharray_values = dasharray_search.group(2).split()
            new_dasharray = ' '.join([str(float(dash) / 25.4) \
                    for dash in dasharray_values])
            new_line = re_stroke_dasharray.sub(r'\1 ' + new_dasharray, line)
        new_css.append(new_line)
    with open(css_dest, 'w') as f:
        f.write(''.join(new_css))

def get_css(dest_dir: Filepath, inches: bool = False) -> None:
    """ Check if css file is in dest_dir and copy it from ASSETS_DIR if needed 
        Then fix units by calling css_to_inches() """
    css_src = ASSETS_DIR / BASE_CSS
    css_dest = dest_dir / BASE_CSS
    if css_dest.is_file():
        print('CSS is already there. Nothing to add')
        return
    print('CSS is not there: copy it')
    if not inches:
        shutil.copy(css_src, dest_dir)
        return
    css_to_inches(css_src, css_dest)

def get_svg_composition(subjects: list['Drawing_subject'], draw_all: bool) \
        -> "pathlib.PosixPath":
    """ Collect every subject svg in a single composed svg 
        or add new subject to existing composed svg """
    ## TODO check why a lot of defs elements are created
    print('Start composition')
    composition_start_time = time.time()

    cam = get_drawing_camera()
    composition = cam.drawing_filepath
    working_scene = get_working_scene()
    print('Filepath:', composition)
    ## TODO try to set cm as display units of svg 
    if not composition.is_file() or draw_all:
        ## Create a new svg and collect all subject in it as use
        abstract_composition = get_abs_svg(working_scene.svg_size, 
                get_css_style(''))
        absoverall_group = AbsLayer(label = cam.name, entity_id = cam.name)
        abstract_composition.add_entity(absoverall_group)
        for layer_label in layers_labels:
            abslayer = AbsLayer(label = layer_label, entity_id = layer_label)
            abslayer.add_class([layer_label, cam.name])
            absoverall_group.add_entity(abslayer)
            for subject in subjects:
                subject_use = subject.get_svg_use(layer_label)
                if not subject_use:
                    continue
                abslayer.add_entity(subject_use)
    else:
        existing_composition = Svg_read(composition).drawing
        existing_subjects = subjects_in_svg(existing_composition, subjects)
        new_subjects = []
        for subj in subjects:
            if subj not in existing_subjects:
                new_subjects.append(subj) 
                continue
            for style in subj.styles:
                if style in existing_subjects[subj]:
                    continue
                new_subjects.append(subj)
        ## TODO fix: adding hidden drawing recreate original too 
        ##      (so it appears twice)
        if new_subjects:
            for style in layers_labels:
                svg_layers = existing_composition.get_entities_by_tag('g', True)
                containers = [i for i in svg_layers if hasattr(i, 'label') and 
                        i.label == style]
                if not containers:
                    continue
                for subject in new_subjects:
                    subject_use = subject.get_svg_use(style)
                    if not subject_use:
                        continue
                    containers[0].add_entity(subject_use)
        abstract_composition = existing_composition
    abstract_composition.to_real(composition)
    inches = working_scene.scene.unit_settings.system == 'IMPERIAL'
    get_css(cam.svg_path.parent.absolute(), inches)
    print(f'\t...completed in {(time.time() - composition_start_time)}\n')
    return composition
