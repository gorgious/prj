#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
import os
import re
import ast
import math
import inspect
from mathutils import Vector, geometry
from prj.utils import flatten, f_to_8_bit, unfold_ranges, ranges, name_cleaner
from prj.utils import get_prj_camera_id, debug_here, get_drawing_styles
from prj.mesh_utils import remove_bad_oriented_faces, cut_by_plane
from prj.mesh_utils import remove_doubles
from prj.drawing_camera import get_drawing_camera
from prj.svg_data import Svg_data
from prj.working_scene import get_working_scene
from prj.svglib import AbsUse

drawing_styles = None
drawing_subjects = []

def get_subjects_list():
    return drawing_subjects

def reset_subjects_list():
    global drawing_subjects
    drawing_subjects.clear()

class Drawing_subject:
    obj_eval: bpy.types.Object
    original_name: str
    mesh_eval: bpy.types.Mesh
    matrix: 'mathutils.Matrix'
    instancer_eval: bpy.types.Object
    is_instance: bool
    lib_path: str
    over_lib_path: str
    cam_bound_box: list[Vector]
    overlapping_subjects: list['Drawing_subject']
    bounding_rect: list[Vector]
    render_pixels: list[int]
    pixels_range: list[tuple[int]]  ## Exery tuple define the start and the end
                                    ## (end included!) of pixels's render lines 
    is_symbol: bool
    symbol_type: str
    back_stairs_obj: bpy.types.Object
    working_scene: 'Working_scene'
    obj: bpy.types.Object
    name: str
    is_selected: bool
    is_in_front: bool
    is_behind: bool
    is_cut: bool
    is_open_cut: bool
    back_drawing: bool
    conditions: dict[str, bool]
    styles: list['Drawing_style']
    xray_drawing: bool
    cut_only_drawing: bool
    hidden_drawing: bool
    draw_outline: bool
    force_drawing: bool
    drawing_camera: 'Drawing_camera'
    svg_data: Svg_data
    previous_data: dict
    previous_render_pixels: list[int]
    collections: list[bpy.types.Collection]
    has_cut: bool
    has_grease_pencil: bool
    grease_pencil_modifier: bpy.types.LineartGpencilModifier
    grease_pencil: bpy.types.Object ## bpy.types.GreasePencil
    stairs_cut_ref: 'Drawing_symbol'
    cut_obj: bpy.types.Object
    obj_cam_id: int

    FULL_NAME_SEP: str = '_-_'
    SVG_DATA_RE = re.compile('(?s:.*)<!--\s(.*)\s-->', re.DOTALL)

    def __new__(cls, *args, **data) -> 'Drawing_subject':
        ## TODO fix symbol duplicated process
        #print('data and function', data['symbol_type'], 
        #        data['instancer_symbol_type'], inspect.stack()[1].function)
        if (data['symbol_type'] or data['instancer_symbol_type']):
            if inspect.stack()[1].function != '__new__':
                print('Create symbol', data['name'])
                from prj.drawing_symbol import Drawing_symbol
                return Drawing_symbol(**data)
        return super(Drawing_subject, cls).__new__(cls)

    def __init__(self, 
            obj_eval: bpy.types.Object, 
            name: str, 
            mesh_eval: bpy.types.Mesh, 
            matrix: 'mathutils.Matrix', 
            instancer_eval: bpy.types.Object, 
            is_instance: bool, 
            lib_path: str, 
            over_lib_path: str, 
            ## The area inside the camera frame where the subject is
            bounding_rect: list[Vector], 
            is_in_front: bool,
            is_behind: bool, 
            back_drawing: True,
            collections: list[bpy.types.Collection],
            symbol_type: str = None, 
            instancer_symbol_type: str = None,
            obj_cam_id: int = 0):

        if self in drawing_subjects:
            return self
        print('Create subject for', name)
        self.working_scene = get_working_scene()
        self.lib_path = lib_path
        self.over_lib_path = over_lib_path
        self.obj_eval = bpy.data.objects[obj_eval.name, lib_path]
        clean_name = name_cleaner(name).lower()
        base_name = ''
        if instancer_eval:
            self.instancer_eval = bpy.data.objects[instancer_eval.name]
            clean_instancer_name = name_cleaner(self.instancer_eval.name).lower()
            base_name += f"{clean_instancer_name}{self.FULL_NAME_SEP}"
        else:
            self.instancer_eval = instancer_eval
        full_name = base_name + clean_name
        self.original_name = name
        self.mesh_eval = mesh_eval
        self.matrix = matrix
        self.is_instance = is_instance
        self.bounding_rect = bounding_rect
        self.overlapping_subjects = []
        self.render_pixels = []
        self.pixels_range = []
        self.is_symbol = bool(symbol_type) or bool(instancer_symbol_type)
        self.symbol_type = instancer_symbol_type if not symbol_type \
                else symbol_type
        self.back_stairs_obj = None
        self.cut_obj = None
        self.drawing_camera = get_drawing_camera()
        self.cam_id = obj_cam_id 
        self.area_pixels = [] if self.drawing_camera.disable_render else \
                self.get_area_pixels() 

        ## Move a no-materials duplicate to working_scene: materials could 
        ## bother lineart (and originals are kept untouched)
        self.obj = bpy.data.objects.new(name=full_name, 
                object_data=self.mesh_eval)
        remove_doubles(self.obj)
        self.name = name_cleaner(self.obj.name)
        self.obj.matrix_world = self.matrix
        self.obj.data.materials.clear()
        self.working_scene.link_object(self.obj)
        self.obj.hide_viewport = True

        self.is_selected = False
        self.is_in_front = is_in_front
        self.is_behind = is_behind
        self.is_cut = self.is_in_front and self.is_behind
        self.is_open_cut = False
        self.back_drawing = back_drawing
        self.conditions = self.get_conditions()
        global drawing_styles
        drawing_styles = get_drawing_styles()
        self.styles = [drawing_styles[s] for s in drawing_styles \
                if drawing_styles[s].default]
        if self.xray_drawing:
            self.styles.append(drawing_styles['x'])
        if self.hide_projected or not self.is_in_front:
            self.styles.remove(drawing_styles['p'])
        if self.hidden_drawing:
            self.styles.append(drawing_styles['h'])
        if self.is_cut:
            self.styles.append(drawing_styles['c'])
        if self.back_drawing:
            self.styles.append(drawing_styles['b'])
        if self.is_symbol:
            self.styles = [drawing_styles['s']]
        if self.cut_only_drawing:
            self.styles = [drawing_styles['c']]

        self.svg_data = Svg_data(path=self.get_svg_path(), subject=self)
        self.previous_data = self.__get_previous_data()
        self.previous_render_pixels = None if not self.previous_data \
                else unfold_ranges(self.previous_data['render_pixels'])

        self.collections = collections
        self.has_cut = False
        self.has_grease_pencil = False
        self.grease_pencil_modifier = None
        self.grease_pencil = None
        self.stairs_cut_ref = None
        self.description = self.obj_eval.prj_drawing_settings.object_description
        self.tags = self.obj_eval.prj_drawing_settings.object_tags
        drawing_subjects.append(self)

    def has_back_stairs(self):
        return bool(self.back_stairs_obj)
    
    def get_grease_pencil(self) -> bpy.types.Object:
        return self.grease_pencil

    def set_grease_pencil(self, gp: bpy.types.Object) -> None:
        self.grease_pencil = gp
        self.has_grease_pencil = True

    def remove_grease_pencil(self) -> None:
        if not self.grease_pencil:
            return
        bpy.data.grease_pencils.remove(self.grease_pencil.data)
        self.set_grease_pencil(None)

    def add_gp_modifier(self, gp_mod: bpy.types.LineartGpencilModifier) -> None:
        self.grease_pencil_modifier = gp_mod

    def remove_gp_modifier(self, 
            gp_mod: bpy.types.LineartGpencilModifier = None) -> None:
        if not self.grease_pencil_modifier:
            return
        gp_mod = gp_mod if gp_mod else self.grease_pencil_modifier
        ## Remove material from lineart to avoid error
        gp_mod.target_material = None
        self.grease_pencil.grease_pencil_modifiers.remove(gp_mod)
        self.grease_pencil_modifier = None

    def get_obj_to_draw(self, style: 'Drawing_style') -> bpy.types.Object:
        """ Depending on style, obj to draw is returned """
        if style.style == 'c' and self.cut_obj:
            return self.cut_obj
        if style.style == 'b' and self.has_back_stairs():
            return self.back_stairs_obj
        return self.obj

    def create_cut(self) -> bpy.types.Object:
        """ Cut the subject using the camera frame and create a plane obj """
        cam = self.drawing_camera
        plane_co = cam.frame[0]
        plane_no = geometry.normal(cam.frame)
        self.cut_obj = cut_by_plane(self.obj, plane_co, plane_no, False)
        if len(self.cut_obj.data.polygons) == 0:
            self.is_open_cut = True

        self.has_cut = True
        return self.cut_obj

    def get_visible_mesh(self) -> None:
        """ Remove all faces that are pointing opposite to the camera """
        if self.has_back_stairs():
            print('simplify back stairs')
            self.back_stairs_obj = remove_bad_oriented_faces(
                    self.back_stairs_obj, self.drawing_camera.direction)
            return
        self.obj = remove_bad_oriented_faces(self.obj, 
                self.drawing_camera.direction)

    def get_conditions(self) -> dict[str, bool]:
        """ Return subject's prj_drawing_settings 
            (and assign them to respective single variables) """
        if not self.obj_eval.prj_camera:
            obj_draw_settings = self.obj_eval.prj_drawing_settings
        elif self.instancer_eval and self.cam_id > 0:
            obj_draw_settings = self.instancer_eval.prj_camera[self.cam_id]
        else:
            obj_draw_settings = self.obj_eval.prj_camera[self.cam_id]
        if not self.instancer_eval:
            self.xray_drawing = obj_draw_settings.xray
            self.cut_only_drawing = obj_draw_settings.cut_only
            self.hide_projected = obj_draw_settings.hide_projected
            self.hidden_drawing = obj_draw_settings.hidden
            self.draw_outline = obj_draw_settings.outline
            self.force_drawing = obj_draw_settings.force
        else:
            instancer_draw_settings = self.instancer_eval.prj_camera[self.cam_id]
            self.xray_drawing = obj_draw_settings.xray or \
                    instancer_draw_settings.xray
            self.cut_only_drawing = obj_draw_settings.cut_only or \
                    instancer_draw_settings.cut_only
            self.hide_projected = obj_draw_settings.hide_projected or \
                    instancer_draw_settings.hide_projected
            self.hidden_drawing = obj_draw_settings.hidden or \
                    instancer_draw_settings.hidden
            self.draw_outline = obj_draw_settings.outline or \
                    instancer_draw_settings.outline or self.back_drawing
            self.force_drawing = obj_draw_settings.force or \
                    instancer_draw_settings.force
        return {'force': self.force_drawing,
                'outline': self.draw_outline, 'hidden': self.hidden_drawing,
                'back': self.back_drawing, 'symbol': self.is_symbol}

    def __get_previous_data(self) -> dict:
        """ Get data stored in last subject svg """
        try:
            prev_svg = open(self.svg_data.path, 'r')
            prev_svg_content = prev_svg.read()
            prev_svg.close()
            subject_data_search = re.search(self.SVG_DATA_RE, prev_svg_content)
            if not subject_data_search:
                print ('No previous data for', self.name)
                return None
            subject_data_raw = subject_data_search.groups(1)[0]
            previous_data = ast.literal_eval(subject_data_raw)
            return previous_data
        except OSError:
            print (f"{self.svg_data.path} doesn't exists")
            return None
        except ValueError:
            pass

    def __repr__(self) -> str:
        return f'Drawing_subject[{self.name}]'

    def set_color(self, rgba: tuple[float]) -> None:
        """ Assign rgba color to object """
        r, g, b, a = rgba
        self.obj.color = rgba
        self.color = (f_to_8_bit(r), f_to_8_bit(g), f_to_8_bit(b), f_to_8_bit(a))

    def get_svg_path(self, prefix: str = None, suffix: str = None) -> str:
        """ Return the svg filepath with prefix or suffix """
        path = self.drawing_camera.path
        sep = "" if path.endswith(os.sep) else os.sep
        pfx = f"{prefix}_" if prefix else ""
        sfx = f"_{suffix}" if suffix else ""
        filename = f'{pfx}{self.name}{sfx}'
        svg_path = f"{path}{sep}{filename}.svg"
        return svg_path

    def link_data(self, data: bpy.types.Mesh) -> None:
        self.obj.data = data
    
    def add_overlapping_subjs(self, subjects: list['Drawing_subject']) -> None:
        """ Add subjects to self.overlapping_subjects """
        for subj in subjects:
            if subj == self or subj in self.overlapping_subjects:
                continue
            self.overlapping_subjects.append(subj)

    def get_overlap_subjects(self, subjects: list['Drawing_subject']) -> None:
        """ Populate self.overlapping_subjects with subjects that overlaps in
            frame view (by bounding_rect) and add self to those subjects too """
        for subject in subjects:
            if subject == self:
                continue
            if subject in self.overlapping_subjects:
                continue
            for vert in self.bounding_rect:
                intersect_point = geometry.intersect_point_quad_2d(vert,
                        *subject.bounding_rect)
                if intersect_point:
                    self.add_overlapping_subjs([subject])
                    subject.add_overlapping_subjs([self])
                    break
            for vert in subject.bounding_rect:
                intersect_point = geometry.intersect_point_quad_2d(vert,
                        *self.bounding_rect)
                if intersect_point:
                    self.add_overlapping_subjs([subject])
                    subject.add_overlapping_subjs([self])
                    break
            for i in range(4):
                line_self = (self.bounding_rect[i%4], 
                        self.bounding_rect[(i+1)%4])
                for j in range(4):
                    line_subj = (subject.bounding_rect[j%4], 
                            subject.bounding_rect[(j+1)%4])
                    intersection = geometry.intersect_line_line_2d(line_self[0], 
                            line_self[1], line_subj[0], line_subj[1])
                    if intersection:
                        self.add_overlapping_subjs([subject])
                        subject.add_overlapping_subjs([self])
                        break

    def get_area_pixels(self) -> list[int]:
        """ Get the pixel number (int) of the subject bounding rect area """
        base_resolution = self.working_scene.get_resolution()
        resolution_percent = self.working_scene.resolution_percentage
        resolution = [int(resolution_percent * v) for v in base_resolution]
        bound_rect_x = self.bounding_rect[0].x
        bound_rect_y = self.bounding_rect[2].y
        bound_width = self.bounding_rect[2].x - self.bounding_rect[0].x
        bound_height = self.bounding_rect[2].y - self.bounding_rect[0].y
        px_from_x = math.floor(resolution[0] * bound_rect_x)
        px_from_y = resolution[1] - math.ceil(resolution[1] * bound_rect_y)
        px_width = math.ceil(resolution[0] * bound_width)
        px_height = math.ceil(resolution[1] * bound_height)
        pixels = flatten([list(range(px_from_x+(resolution[0]*y), 
            px_from_x+(resolution[0]*y)+px_width))
            for y in range(px_from_y, px_from_y + px_height)])
        return pixels

    def add_render_pixels(self, pixels: list[int]) -> None:
        """ Assign pixels to relative subject paramenters """
        self.render_pixels = [pixel for pixel in pixels]
        self.pixels_range = list(ranges(pixels))

    def set_stairs_cut(self, cut_symbol: 'Drawing_symbol') -> None:
        self.stairs_cut_ref = cut_symbol
        if drawing_styles['c'] in self.styles:
            self.styles.remove(drawing_styles['c'])

    def get_svg_classes(self) -> list[str]:
        """ Get css classes for subject """
        classes = [cond for cond in self.conditions if self.conditions[cond]]
        classes += [self.name]
        classes += [name_cleaner(coll.name) for coll in self.collections]
        classes += [self.symbol_type] if self.is_symbol else []
        classes += [tag.strip() for tag in self.tags.split(',')] \
                if self.tags else []
        classes += ['open_cut'] if self.is_open_cut else []
        return classes

    def get_svg_id(self, style: str) -> str:
        return f'{self.name}_{style}__{self.drawing_camera.name}'

    def get_svg_use(self, style) -> AbsUse:
        if style not in [s.name for s in self.styles]:
            return
        use_id = self.get_svg_id(style)

        basepath_length = len(str(self.working_scene.render_basepath))
        relative_link = self.svg_data.path[basepath_length:]
        link = f'{relative_link}#{use_id}'
        new_use = AbsUse(link, entity_id = use_id)
        new_use.set_attribute({'xlink:title': self.original_name})

        classes = [style] + self.get_svg_classes() + [self.drawing_camera.name]
        classes += [self.get_svg_id(style)]
        for css_class in classes:
            new_use.add_class(css_class)

        ## TODO extend use of title and description to every object in svglib
        new_use.set_desc(title=self.original_name, desc=self.description)
        return new_use

    def remove(self):
        """ Delete subject """
        if self in drawing_subjects:
            drawing_subjects.remove(self)
        self.working_scene.unlink_object(self.obj)
        bpy.data.meshes.remove(self.obj.data)
        self.remove_grease_pencil()
        self.set_grease_pencil(None)
        self.svg_data.remove()
        del self

