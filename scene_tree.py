#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy

the_scene_tree = None

def get_scene_tree(scene_collection: bpy.types.Collection = None) \
        -> 'Scene_tree':
    global the_scene_tree
    if the_scene_tree:
        return the_scene_tree
    scene_tree = Scene_tree(scene_collection)
    the_scene_tree = scene_tree.tree
    return the_scene_tree

class Scene_tree:
    tree: dict[tuple[int], bpy.types.Collection]
        ## bpy.types.Collection or bpy.types.Object

    def __init__(self, scene_collection: bpy.types.Collection):
        self.tree = self.get_collection_tree(collection=scene_collection, 
                scene_tree={})

    def get_collection_tree(self, collection: bpy.types.Collection, 
            scene_tree: dict = {}, position: tuple[int] = (0,)) \
                    -> dict[tuple[int], bpy.types.Collection]:
        """ Populate scene_tree dict by collection's collections and objects """
        scene_tree.update({position: collection})
        i = 0
        for child in collection.children:
            self.get_collection_tree(child, scene_tree, position + (i,))
            i += 1
        for j, obj in enumerate(collection.objects, start=i):
            scene_tree.update({position + (j,): obj})
        return scene_tree

