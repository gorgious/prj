#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
import time
from prj.utils import get_render_data, flatten, debug_here
from prj.working_scene import get_working_scene, Working_scene
from prj.drawing_camera import get_drawing_camera
from prj.progress_indicator import get_progress_indicator

def get_render_groups(subjects: list['Drawing_subject'], 
        render_groups: list) -> list[list['Drawing_subject']]:
    """ Create groups of not-overlapping subjects (by bounding rect) 
        and return them in a list """
    ## Don't set default value for render_groups in order to avoid keeping
    ## subjects (with removed obj) between script runs
    if not subjects:
        return render_groups
    if len(subjects) < 2:
        render_groups.append(subjects)
        return render_groups
    separated_subjects = [subj for subj in subjects[1:] \
            if subj not in subjects[0].overlapping_subjects]
    for subj in separated_subjects[:]:
        ## separate_subjs changes during the loop so check if subj is still there
        if subj not in separated_subjects:
            continue
        for over_sub in subj.overlapping_subjects:
            if over_sub not in separated_subjects:
                continue
            separated_subjects.remove(over_sub)
    render_groups.append([subjects[0]] + separated_subjects)
    next_subjects = [subj for subj in subjects 
            if subj not in flatten(render_groups)]
    return get_render_groups(next_subjects, render_groups)

def render_separate_subjects(render_groups: list[list['Drawing_subject']]) \
                -> dict[tuple['Drawing_subject'], 'Imaging_Core']:
    """ Execute combined renderings to map pixels to 
        subjects in render_groups """
    subj_pixels = {}
    ### Prepare the scene
    drawing_camera = get_drawing_camera()
    working_scene = get_working_scene()
    render_scene = Working_scene(scene_name='prj_rnd', 
            resolution=working_scene.get_resolution(), 
            resolution_percentage=working_scene.resolution_percentage, 
            camera=drawing_camera.obj)
    ### Execute renderings and map pixels
    renders_time = time.time()
    
    progress = get_progress_indicator()
    progress.set_phase(phase_data={'name': 'render_separate_subjects',
                'start':progress.get_last_update(), 
                'end':progress.get_next_step('main'),'steps':len(render_groups)})

    for i, subjects_group in enumerate(render_groups):
        objs = [subj.obj for subj in subjects_group]
        render_pixels = get_render_data(objs, render_scene.scene)
        subj_pixels[tuple(subjects_group)] = render_pixels
        progress.update(phase_name='render_separate_subjects', step=i)
    render_scene.remove()
    print('Render selected_subjects in', time.time() - renders_time)
    return subj_pixels

def set_subjects_overlaps(subjects: list['Drawing_subject']) -> None:
    """ Define overlapping subjects (based on actual pixels) """
    subjects_map: dict[int, list['Drawing_subject']] = {}
    for i, subj in enumerate(subjects):
        ## Clear overlapping objects based on bounding rect
        subj.overlapping_subjects = []
        ## Create a dict mapping every pixel to subjects that occupies it
        for pixel in subj.render_pixels:
            if pixel not in subjects_map:
                subjects_map[pixel] = []
            subjects_map[pixel].append(subj)
    ## Rewrite subject.overlapping_subjects
    for pixel in subjects_map:
        if len(subjects_map[pixel]) < 2:
            ## pixel is occupied by just one subject
            continue
        ## pixel has multiple subjects overlapping
        for subj in subjects_map[pixel]:
            subj.add_overlapping_subjs(subjects_map[pixel])

def get_overlaps(subjects: list['Drawing_subject'])  -> None:
    """ Calculate exact overlaps between subjects """

    drawing_camera = get_drawing_camera()
    ## TODO add maybe_occluded property to Drawing_subject
    maybe_occluded = [subj for subj in subjects if not subj.is_symbol \
            and subj.is_in_front] ## back subjects are not occluding


    progress = get_progress_indicator()
    progress.set_phase(phase_data={
        'name': 'get_overlaps', 'start':progress.get_last_update(), 
        'end':progress.get_next_step('main'), 'steps':len(maybe_occluded)})
    
    ## Calculate overlaps for every visible subject (based on bounding box)
    for i, subj in enumerate(maybe_occluded):
        progress.update(phase_name='get_overlaps', step=i)
        subj.get_overlap_subjects(maybe_occluded)

    ## Leave subjects overlaps based on bounding box
    if drawing_camera.disable_render:
        return
    
    ## Assign to every subject its actual pixels to get precise overlappings

    ### Get groups of not-overlapping subjects to perfom combined renderings
    render_groups = get_render_groups(subjects=maybe_occluded, render_groups=[])
    groups_pixels = render_separate_subjects(render_groups)

    for group in groups_pixels:
        for subj in group:
            subj_pixels = [pixel for pixel in subj.area_pixels \
                    if groups_pixels[group][pixel][3] == 255]
            subj.add_render_pixels(subj_pixels)
    # Define exact overlaps for every subject and add overlapping subjects to
    # every subject
    set_subjects_overlaps(maybe_occluded)
