#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import os
from prj.svglib import AbsSvg_drawing, AbsStyle, AbsLayer, AbsPath
from prj.utils import name_cleaner, debug_here
from prj.drawing_camera import get_drawing_camera

SVG_ID = 'svg'

def get_abs_svg(svg_size: str, css_style: str = None, layers: list[str] = None) \
        -> AbsSvg_drawing:
    """ Create an abstract svg with css style and a layers """
    abssvg = AbsSvg_drawing(size=svg_size, entity_id = SVG_ID)
    if css_style:
        absstyle = AbsStyle(content = css_style) 
        abssvg.add_entity(absstyle)

    if layers:
        for layer in layers:
            abslayer = AbsLayer(label = layer)
            abssvg.add_entity(abslayer)

    return abssvg

def transform_points(points:list[tuple[float]], factor: float = 1, 
        rounding: int = 16) -> list[tuple[float]]:
    """ Scale (by factor) and round points (by rounding) """ 
    new_points = []
    for coords in points:
        new_coord = tuple([round(co*factor, rounding) for co in coords])
        new_points.append(new_coord)
    return new_points

def get_path_coords(coords: list[tuple[float]]) -> str:
    """ Return the coords as string for paths """
    closed = coords[0] == coords[-1]
    string_coords = 'M '
    for co in coords[:-1]:
        string_coords += f'{str(co[0])},{str(co[1])} '
    closing = 'Z ' if closed else f'{str(coords[-1][0])},{str(coords[-1][1])} '
    string_coords += closing
    return string_coords

def join_coords(coords: list[list[tuple[float]]]) -> list[list[tuple[float]]]:
    """ Join coords list (as from polyline) and put new coords lists in seqs """
    seqs = []
    for coord in coords:
        seqs = add_tail(seqs, coord)
    return seqs

def add_tail(sequences: list[list[tuple[float]]], tail: list[tuple[float]]) -> \
        list[list[tuple[float]]]:
    """ Add tail to sequences and join it to every sequence 
        whith corresponding ends """
    to_del = []
    new_seq = tail
    last_joined = None
    seqs = [seq for seq in sequences for t in [0, -1]]
    for i, seq in enumerate(seqs):
        t = -(i%2) ## -> alternate 0 and -1
        ends = [seq[0], seq[-1]]
        if new_seq[t] not in ends or last_joined == seq:
            continue
        index = -ends.index(new_seq[t]) ## -> 0 | 1
        step = (-2 * index) - 1 ## -> -1 | 1
        val = 1 if t == 0 else -1 ## -> 1 | -1 | 1 | -1
        ## Cut first or last and reverse f necessary
        seq_to_check = new_seq[1+t:len(new_seq)+t][::step*val]
        ## Compose accordingly
        new_seq = [ii for i in [seq,seq_to_check][::step] for ii in i]
        last_joined = seq
        if seq not in to_del:
            to_del.append(seq)
    for s in to_del:
        sequences.remove(s)
    sequences.append(new_seq)
    return sequences

def subjects_in_svg(abstract_svg: AbsSvg_drawing,
        subjects: list['Drawing_subjects']) \
                -> dict['Drawing_subjects', list['Drawing_style']]:
    """ Get the subjects in the abstract_svg and their styles """
    use_objects = abstract_svg.get_entities_by_tag('use', True)
    use_ids = [use.attributes['id'] for use in use_objects]
    subjects_in_svg = {}
    for subject in subjects:
        for style in subject.styles:
            subject_svg_id = subject.get_svg_id(style.name)
            if subject_svg_id not in use_ids:
                continue
            if subject not in subjects_in_svg:
                subjects_in_svg[subject] = []
            subjects_in_svg[subject].append(style)
    return subjects_in_svg
    
