#    Copyright (C) 2022  Marco Ferrara
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

import bpy
import gpu
from gpu_extras.batch import batch_for_shader
from mathutils import Vector
from prj.utils import cam_frame_factors
from bpy.app.handlers import persistent

handler = None
on = False

## TODO Add comments and type hints

def get_drawing_limits_verts(camera, camera_matrix):
    half_frame = camera.ortho_scale/2
    shift_x = camera.shift_x * camera.ortho_scale
    shift_y = camera.shift_y * camera.ortho_scale
    half_frame_x = half_frame * cam_frame_factors(camera)['x']
    half_frame_y = half_frame * cam_frame_factors(camera)['y']
    cam_start = camera.clip_start + .01
    frame_local_verts = [
            Vector((-half_frame_x+shift_x,-half_frame_y+shift_y,-cam_start)),
            Vector((half_frame_x+shift_x,-half_frame_y+shift_y,-cam_start)),
            Vector((-half_frame_x+shift_x,half_frame_y+shift_y,-cam_start)),
            Vector((half_frame_x+shift_x,half_frame_y+shift_y,-cam_start))]
    frame_verts = [camera_matrix @ v for v in frame_local_verts]
    return tuple([(v.x, v.y, v.z) for v in frame_verts])

class PRJ_OT_drawing_limits(bpy.types.Operator):
    """Handle camera drawing limits"""
    bl_idname = "prj.drawing_limits"
    bl_label = "Show drawing_limits"
    bl_options = {'REGISTER', 'UNDO'}

    indices: tuple[tuple]
    vertices: tuple[tuple]
    shader: gpu.shader
    camera_name: bpy.props.StringProperty(name="Camera name")
    startup: bpy.props.BoolProperty(name="Startup")

    _timer = None

    def __init__(self):
        self.indices = ((0, 1), (1, 3), (2, 3), (2, 0))
        self.shader = gpu.shader.from_builtin('3D_UNIFORM_COLOR')
        self.camera = bpy.data.cameras[self.camera_name]
        self.on = False

    def __del__(self):
        if self._timer:
            wm = self.context.window_manager
            wm.event_timer_remove(self._timer)

    def draw_limits(self, batch):
        self.shader.bind()
        self.shader.uniform_float("color", (1, 0, 0, 1))
        batch.draw(self.shader)

    def draw_drawing_limits(self):
        global handler
        self.remove_handler()
        batch = batch_for_shader(self.shader, 'LINES', {"pos": self.vertices}, 
                indices=self.indices)
        handler = bpy.types.SpaceView3D.draw_handler_add(self.draw_limits, 
                (batch,), 'WINDOW', 'POST_VIEW')

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return context.area.type in ['VIEW_3D', 'PROPERTIES']

    def execute(self, context):
        self.vertices = get_drawing_limits_verts(self.camera, self.cam_matrix)
        self.draw_drawing_limits()
        return {'FINISHED'}

    def modal(self, context, event):
        ## Stop the operator if camera view is changed 
        ## and disable show_drawing_limits
        if self.region_3d.view_perspective != 'CAMERA':
            context.window_manager.event_timer_remove(self._timer)
            self.remove_handler()
            return {'FINISHED'}
        elif not on:
            ## Update screen with drawing limits by activating camera view again
            self.region_3d.view_perspective = 'CAMERA'
            context.window_manager.event_timer_remove(self._timer)
            self.remove_handler()
            return {'FINISHED'}
        ## Update drawing limits if camera change position or properties
        elif self.is_camera_changed():
            self.update_cam_data()
            self.execute(context)
        return {'PASS_THROUGH'}

    def is_camera_changed(self) -> bool:
        if self.cam_data['matrix'] == self.cam_matrix \
                and self.cam_data['shift_x'] == self.camera.shift_x \
                and self.cam_data['shift_y'] == self.camera.shift_y \
                and self.cam_data['clip_start'] == self.camera.clip_start \
                and self.cam_data['ortho_scale'] == self.camera.ortho_scale:
                    return False
        return True

    def update_cam_data(self) -> None:
        self.cam_data = {
                'matrix': self.cam_matrix.copy(),
                'shift_x': self.camera.shift_x,
                'shift_y': self.camera.shift_y,
                'clip_start': self.camera.clip_start,
                'ortho_scale': self.camera.ortho_scale,
                }

    def remove_handler(self):
        global handler
        if handler:
            bpy.types.SpaceView3D.draw_handler_remove(handler, 'WINDOW')
            handler = None

    def get_region_3d(self, context):
        r3d = None
        for area in context.workspace.screens[0].areas:
            if area.type != 'VIEW_3D':
                continue
            view_3d = area.spaces[0].region_3d.view_perspective
            r3d = area.spaces[0].region_3d
            break
        return r3d

    def invoke(self, context: bpy.types.Context, event: bpy.types.Event):
        self.context = context

        ## Camera has to be the active object
        obj = context.active_object
        if not obj or obj.data != self.camera:
            return {'FINISHED'}

        ## A region 3d area is needed
        self.region_3d = self.get_region_3d(context)
        if not self.region_3d:
            error_message = 'A 3D Viewport is needed to show drawing limits'
            self.report({'WARNING'}, error_message)
            return {'FINISHED'}

        global on
        if self.startup:
            ## Command has been triggered by drawing_limits_op (not by ratio)
            on = not on
            ## If not in camera view activate it, otherwise update screen 
            ## by reactivating it
            self.region_3d.view_perspective = 'CAMERA'
            if not on:
                ## Drawing limits are already active: disable them
                return {'RUNNING_MODAL'}
        else:
            ## Command has been triggered by drawing_ratio
            if self.region_3d.view_perspective != 'CAMERA':
                return {'FINISHED'}
            if not on:
                return {'FINISHED'}

        self.cam_matrix = context.active_object.matrix_world
        self.update_cam_data()

        ## TODO handle triggering of multiple timers if any
        wm = context.window_manager
        self._timer = wm.event_timer_add(time_step=0.1, window=context.window)

        self.execute(context)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}


@persistent
def disable_drawing_limits(none):
    """ Remove drawing limits handler """
    global handler
    if handler:
        bpy.types.SpaceView3D.draw_handler_remove(handler, 'WINDOW')
        handler = None

bpy.app.handlers.save_pre.append(disable_drawing_limits)

classes = (PRJ_OT_drawing_limits,)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "prj.drawing_limits":
    register()
