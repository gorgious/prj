## vX.X.X
- Add custom XDATA to DXF
- Figure out how to draw intersection between subjects
- Add properties to scene:
	- BoQ_base_ref.filepath (bill of quantities)   
		 -> file contains ids, descriptions, units, unit rates
- Add properties to objects:
	- BoQ.id
	- BoQ.quantity (with auto-calc if selected faces or edges)
	
## Communication
- Example of architectural model and drawings
- Guide on wiki 
