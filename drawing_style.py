#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import itertools
from prj.cad import get_default_lineweight

position = itertools.count()
## Last style will be above the others, the first one will be under the others
STYLES = {
        'p': {'name': 'prj', 'default': True, 'occlusion_start': 0, 
            'occlusion_end': 0, 'position': next(position),
            'dxf_linetype': 'CONTINUOUS', 
            'dxf_lineweight': get_default_lineweight(), 'dxf_color': 8},
        'c': {'name': 'cut', 'default': False, 'occlusion_start': 0, 
            'occlusion_end': 128, 'position': next(position),
            'dxf_linetype': 'CONTINUOUS', 
            'dxf_lineweight': get_default_lineweight(), 'dxf_color': 253},
        'x': {'name': 'xray', 'default': False, 'occlusion_start': 0, 
            'occlusion_end': 0, 'position': next(position),
            'dxf_linetype': 'DASHED', 'dxf_lineweight': get_default_lineweight(), 
            'dxf_color': 251},
        'h': {'name': 'hid', 'default': False, 'occlusion_start': 1, 
            'occlusion_end': 128, 'position': next(position),
            'dxf_linetype': 'HIDDEN', 'dxf_lineweight': get_default_lineweight(), 
            'dxf_color': 251},
        's': {'name': 'sym', 'default': False, 'occlusion_start': 0, 
            'occlusion_end': 128, 'position': next(position),
            'dxf_linetype': 'CONTINUOUS', 
            'dxf_lineweight': get_default_lineweight(), 'dxf_color': 251},
        'b': {'name': 'bak', 'default': False, 'occlusion_start': 0, 
            'occlusion_end': 128, 'position': next(position),
            'dxf_linetype': 'DASHED', 'dxf_lineweight': get_default_lineweight(), 
            'dxf_color': 251},
        }

drawing_styles = {}

def get_drawing_styles() -> dict[str, 'Drawing_style']:
    """ Populate drawing_styles dict with Drawing_style objects """
    if drawing_styles:
        return drawing_styles
    for s in STYLES:
        drawing_styles[s] = Drawing_style(style=s, name=STYLES[s]['name'], 
                default=STYLES[s]['default'],
                occlusion_start=STYLES[s]['occlusion_start'],
                occlusion_end=STYLES[s]['occlusion_end'],
                dxf_linetype=STYLES[s]['dxf_linetype'],
                dxf_lineweight=STYLES[s]['dxf_lineweight'],
                dxf_color=STYLES[s]['dxf_color'],
                position=STYLES[s]['position'])
    return drawing_styles

class Drawing_style:

    def __init__(self, style: str, name: str, default: bool,
            occlusion_start: int, occlusion_end: int, 
            dxf_linetype: str, dxf_lineweight: int, dxf_color: int,
            position: int):
        self.style = style
        self.name = name
        self.default = default
        self.occlusion_start = occlusion_start
        self.occlusion_end = occlusion_end
        self.position = position
        self.dxf_linetype = dxf_linetype
        self.dxf_lineweight = dxf_lineweight
        self.dxf_color = dxf_color

