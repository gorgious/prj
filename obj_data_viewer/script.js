var move_to_cursor = null;
var popup_x_offset = -20;
var popup_y_offset = 20;

function GetURLParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function hookSvg(elementID) { 
	//Hook in the contentDocument of the svg so we can fire its internal scripts
	var _u = 'undefined';
	var _f = 'function';
	var svgdoc, svgwin, returnvalue = false;
	var object = (typeof elementID === 'string' ? 
		document.getElementById(elementID) : elementID);
	if (object && object.contentDocument) {
		svgdoc = object.contentDocument;
	}
	else {
		if (typeof object.getSVGDocument == _f) {
			try {
				svgdoc = object.getSVGDocument();
			} catch (exception) {
				// console.log('Neither the HTMLObjectElement nor the 
				// GetSVGDocument interface are implemented');
			}
		}
	}
    if (svgdoc && svgdoc.defaultView) {
		svgwin = svgdoc.defaultView;
    }
    else if (object.window) {
		svgwin = object.window;
    }
    else {
       if (typeof object.getWindow == _f) {
		   try {
			   svgwin = object.getWindow();//TODO look at fixing this 
           }
           catch (exception) {
			   // console.log('The DocumentView interface is not supported\r\n 
			   // Non-W3C methods of obtaining "window" also failed');
           }
       }
    }
    //console.log('svgdoc is ' + svgdoc + ' and svgwin is ' + svgwin);
    if (typeof svgwin === _u || typeof svgwin === null) {
		returnvalue = null;
    } else {
		returnvalue = svgwin;
    }
    return returnvalue;
};

function get_svg_object() {
	objects = document.getElementsByTagName('object');
	for (i=0; i<objects.length; i++) {
		if (objects[i].type != 'image/svg+xml') {
			continue;
		}
		var svg_obj = objects[i];
		break;
	}
	return svg_obj;
}

function getSvgContent(svg, svgTag) {
	var use_objects = [];
	//var svg = hookSvg(get_svg_object());
	var use_elements = svg.document.getElementsByTagName(svgTag);
	for (i=0; i<use_elements.length; i++){
		title = '';
		description = '';
		for (j=0; j<use_elements[i].children.length; j++){
			item = use_elements[i].children[j];
			if (item.tagName == 'title') {
				use_title = item.innerHTML;
			}
			if (item.tagName == 'desc') {
				use_description = item.innerHTML;
			}
		}

		use_object = {
			obj: use_elements[i],
			title: use_title,
			desc: use_description,
		};
		use_objects.push(use_object);
	}
	return use_objects
}

function create_use_popup(use_objs) {
	for (i=0; i<use_objs.length; i++) {
		var span_title = document.createElement("span");
		var obj_title = document.createTextNode(use_objs[i].title);
		span_title.id = 'span_' + use_objs[i].title;
		span_title.classList.add('use_title');
		span_title.appendChild(obj_title);

		var span_desc = document.createElement("span");
		var obj_desc = document.createTextNode(use_objs[i].desc);
		span_desc.id = 'span_desc' + use_objs[i].title;
		span_desc.classList.add('use_desc');
		span_desc.appendChild(obj_desc);

		var use_div = document.createElement("div");
		var span_separator = document.createElement("hr");
		var container = document.getElementById("body");
		use_div.id = 'div_' + use_objs[i].title;
		use_div.classList.add('popup');
		use_div.appendChild(span_title);
		use_div.appendChild(span_separator);
		use_div.appendChild(span_desc);
		container.appendChild(use_div);
		// Add div to actual use object as property
		use_objs[i].obj.ref = use_div;
	}
}

function show_details(use, obj) {
	let x = 0;
	let y = 0;
	use.addEventListener('mousemove', function move_to_cursor(e) {
		x = e.offsetX + popup_x_offset;
		y = e.offsetY + popup_y_offset;
		obj.style.top = y + "px";
		obj.style.left = x + "px";
	});
	obj.style.display = "inline-block";
	obj.style.position = "absolute";
}
function hide_details(use, obj) {
	use.removeEventListener('mousemove', move_to_cursor);
	move_to_cursor = null;
	obj.style.display = "none";
	obj.style.position = "absolute";
	obj.style.top = "-99999px";
	obj.style.left = "-99999px";
}


function init() {
	// Get use object from svg object and create relative html popup elements
	var svg = hookSvg(svg_object);
	var use_elements = getSvgContent(svg, 'use');
	create_use_popup(use_elements);
	for (i=0; i<use_elements.length; i++) {
		use_elements[i].obj.addEventListener("mouseover", 
		 	function() {show_details(this, this.ref);}
		);
		use_elements[i].obj.addEventListener("mouseout", 
		 	function() {hide_details(this, this.ref);}
		);
	}
}

