# prj

### What is prj
*`prj`* is a Blender add-on for __generating semantic technical drawings from 3d model__.   
Its main purpose is to *help architects and designer to make building and interior drawings automatically*.

### Disclaimer
*`prj`* is currently **under development and may be subject to changes**: using it on production is on your own responsibility.   

### About prj
*`prj`* generates *__linked data-rich SVG__* drawings.   
In particular:
- **drawings are in SVG format**   
*Scalable Vector Graphics* is a **non-binary** and **non-proprietary format** that can be viewed by a simple browser on any device. **Interactivity** can be implemented on it and its style can be **handled by CSS**. Editing is easy and graphically powerful using both open source source and proprietary softwares.
- **drawings are linked**   
Every 3D object is represented in a single dedicated SVG file and all those files are linked (as `<use>` element) to the main drawing. That allows to **keep files size small** and to limit redrawing to changed objects only.
- **drawings are data-rich**   
Every object **stores information** about it directly inside the SVG. Hence **text searches** are possible over single and multiple drawings.   

*`prj`* is able to produce drawings in *DXF* or *DWG* file format as well.

*`prj`* uses Blender *Line Art* feature and *SVG grease pencil exporter*.

### Why using prj?
*`prj`* __doesn't mean to be a simple drawing tool__: its main aim is to be __a tool to manage projects information by generating data-rich drawings__ (a kind of *BIM drawing tool*).

This is why it generates drawings in SVG format with a linked structure: this way every object in the drawing can return informations about it (product id, size, weight, physical properties, cost, typology, family belonging, etc…)\* and allow user to get, for example, bill of quantities or interactive drawing you can query.   
> *\* It's important to point out that, at the moment, the add-on is not capable to store some data yet (like quantities) but in future it should be able to do it.*

For these reasons it's recommended to complete the drawing process (composing, styling with css, adding annotations and dimensions) using an SVG editor (like the open source [Inkscape](https://inkscape.org/)).   
Anyway, if you don't care to keep these information inside your drawings, you can produce *DXF* or *DWG* files.

In [General flow and process](https://gitlab.com/marzof/prj/-/wikis/General-flow-and-process) section you can find more informations, tips and tricks about the use of *`prj`* in a normal architectural drawing process.

### Documentation
A proper documentation is under construction in the [wiki](https://gitlab.com/marzof/prj/-/wikis/home).   
In the meantime you can check the [*Tips and tricks page*](https://gitlab.com/marzof/prj/-/wikis/Tips-and-tricks) and find some useful suggestion about using *`prj`*


### Getting started
0. Donwload the addon from [here](https://gitlab.com/marzof/prj/-/releases/permalink/latest/downloads/addon) and [install it](https://gitlab.com/marzof/prj/-/wikis/Installation) in Blender   
Remember to install the dependencies to complete the installation (if it doesn't work you can try to launch Blender as administrator)   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/0_install_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/0_install.gif)
1. Prepare the scene you want to draw and make sure that [geometries are correct](https://gitlab.com/marzof/prj/-/wikis/General-advices).   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/1_fix_geom_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/1_fix_geom.gif)
2. Select the camera for the drawing and set its type as *Orthographic*.   
In data properties window you can set other options for the camera such as the drawing scale or the file name.   
Clip start position defines cut plane.   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/2_prepare_camera_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/2_prepare_camera.gif)
3. In order to set the objects drawing properties (like visibility, tagging or symbol type) check the object properties window.   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/3_set_obj_props_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/3_set_obj_props.gif)
4. In the Output properties set the output path. Then, with the camera selected, in data properties window, click on *Draw it!*   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/4_draw_it_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/4_draw_it.gif)
5. Your drawing is ready!   
Look for the svg file in the output path.   
[<img src="https://gitlab.com/marzof/prj/-/wikis/img/getting_started/5_the_drawing_s.gif">](https://gitlab.com/marzof/prj/-/wikis/img/getting_started/5_the_drawing.gif)
6. Use an SVG editor to add dimensions, texts, graphics and create compositions and use css to style your drawing.


<!--

use "force" to draw a not aligned symbol

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/marzof/prj.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:5eec0daabfe6cf8fd3ca1d1dd6ee908d?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

-->
