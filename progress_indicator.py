#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

from prj.utils import debug_here

class Progress_phase():
    start: float
    end: float
    single_step: float
    steps: int
    steps_values: list[float]

    def __init__(self, name: str = None):
        self.name = name
        self.steps = None

    def set_start(self, value) -> None:
        self.start = value

    def get_start(self) -> float:
        if not self.start:
            return 0.
        return self.start

    def set_end(self, value) -> None:
        self.end = value

    def get_end(self) -> float:
        if not self.end:
            return 1.
        return self.end

    def set_steps_values(self) -> None:
        """ Generate a list of (self.steps) values between self.start and 
            self.end """
        interval = self.end - self.start
        self.single_step = interval/self.steps
        self.steps_values = [self.start + (factor * self.single_step) \
                for factor in range(1, self.steps + 1)]

    def get_steps_values(self) -> list[float]:
        if not self.steps_values:
            return [1.]
        return self.steps_values

    def set_steps(self, steps: int) -> None:
        self.steps = max(steps, 1)
        self.set_steps_values()

    def get_steps(self) -> int:
        if not self.steps:
            return 1
        return self.steps

    def set_step(self, step: float) -> None:
        self.single_step = step

    def get_step(self) -> float:
        return self.single_step


the_progress_indicator = None

def get_progress_indicator(wm: 'bpy.types.WindowManager' = None) -> \
        'Progress_indicator':
    """ Return the_progress_indicator (create it if necessary) """
    global the_progress_indicator
    if wm:
        the_progress_indicator = Progress_indicator(wm)
        return the_progress_indicator
    if the_progress_indicator:
        return the_progress_indicator
    the_progress_indicator = Progress_indicator()
    return the_progress_indicator

class Progress_indicator:
    phases: list[Progress_phase]
    phase: Progress_phase
    last_update: float

    def __init__(self, wm: 'bpy.types.WindowManager' = None, begin: bool = True,
            p_min: float = 0., p_max: float = 1.):
        if not wm:
            self.void = True
            return
        self.void = False
        self.wm = wm
        self.phases = []
        self.step = 0.
        self.phase = None
        self.last_update = 0.
        if begin:
            self.begin(p_min, p_max)

    def begin(self, p_min: float = 0., p_max: float = 1.) -> None:
        if self.void:
            return
        self.wm.progress_begin(p_min, p_max)

    def end(self) -> None:
        if self.void:
            return
        self.wm.progress_end()
        global the_progress_indicator
        if self == the_progress_indicator:
            the_progress_indicator = None

    def get_next_step(self, phase_name: str = None):
        if self.void:
            return 0.
        if phase_name:
            self.set_phase(phase_name = phase_name)
        return self.last_update + self.get_phase().get_step()

    def set_phase(self, phase_name: str = None, 
            phase_data: dict[str, float] = None) -> None:
        if self.void:
            return
        if phase_data:
            ph_name = phase_data['name'] if 'name' in phase_data else ''
            self.phase = Progress_phase(name=ph_name)
            self.phase.set_start(phase_data['start'])
            self.phase.set_end(phase_data['end'])
            self.phase.set_steps(phase_data['steps'])
            self.phases.append(self.phase)
        if not phase_name:
            return
        self.phase = self.get_phase(phase_name)

    def get_phase(self, phase_name: str = None) -> Progress_phase:
        if self.void:
            return
        if phase_name and self.phases:
            for phase in self.phases:
                if phase.name != phase_name:
                    continue
                return phase
            return self.phases[0]
        if self.phase:
            return self.phase
        return None

    def get_last_update(self) -> float:
        if self.void:
            return
        return self.last_update

    def update(self, step: int = None, value: float = None, 
            phase_name: str = None) -> float:
        if self.void:
            return
        if phase_name:
            self.set_phase(phase_name)
        phase = self.get_phase()
        if value != None:
            self.wm.progress_update(value)
            #print(f'{round(value*100,2)}%')
            self.last_update = value
            return value
        if step != None:
            value = phase.get_steps_values()[step]
            self.wm.progress_update(value)
            #print(f'{round(value*100,2)}%')
            self.last_update = value
            return value
        value = phase.get_end() if phase else 1.
        self.wm.progress_update(value)
        #print(f'{round(value*100,2)}%')
        self.last_update = value
        return value
