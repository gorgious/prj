#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
import itertools
from prj.svg_utils import transform_points, join_coords, get_path_coords
from prj.svglib import AbsGroup, AbsPath
from prj.utils import debug_here

ROUNDING: int = 5
svgs_data: list['Svg_data'] = []

def reset_svgs_data():
    global svgs_data
    svgs_data = []

class Svg_data:
    ## TODO Find a better name for the class
    path: str
    subject: 'Drawing_subject'
    data = list['Svg_read']

    def __init__(self, path: str, subject: 'Drawing_subject'):
        if path  in svgs_data:
            return
        self.path = path
        self.subject = subject
        self.data = []
        svgs_data.append(self)

    def add_data(self, content: 'Svg_read') -> None:
        """ Add an Svg_read to self.data """
        self.data.append(content)

    def data_to_svg(self, abs_svg: 'prj.svglib.AbsSvg_drawing',
            scale_factor: float = 1. ) -> None:
        """ Fill abs_svg with self data (scale polylines if needed) """
        subject = self.subject
        cam = subject.drawing_camera
        for svg_read in self.data:
            svg_key = svg_read.get_data('style')
            abspaths = polylines_to_abspaths(
                    svg_read.get_entities_by_tag('polyline'), scale_factor, 
                    svg_read.get_data('style') == 'cut')

            if not abspaths:
                continue
            svg_read.set_data('scaled_coords', [points_groups 
                for path in abspaths for points_groups in path.points])
            if not cam.export_svg:
                return

            ## Create the group for the style and add paths and classes to it
            subj_group_id = subject.get_svg_id(svg_key)
            subj_classes = [svg_key, cam.name] + subject.get_svg_classes()
            subj_classes += [subj_group_id]
            absgroup = AbsGroup(entity_id = subj_group_id)
            absgroup.add_class(subj_classes)
            for abspath in abspaths:
                abspath.add_class(subj_classes)
                ## Add all the paths to the layer group
                absgroup.add_entity(abspath)

            ## Get the corresponding layer and add the group to it
            svg_layers = abs_svg.get_entities_by_tag('g', True)
            abslayer = [i for i in svg_layers if hasattr(i, 'label') and 
                    i.label == svg_key][0]
            abslayer.add_entity(absgroup)
            abslayer.add_class([svg_key, cam.name])


    def remove(self) -> None:
        global svgs_data
        if self in svgs_data:
            svgs_data.remove(self)
        del self

def polylines_to_abspaths(polylines: list['prj.svglib.AbsPolyline'], 
    factor: float = 1., closed: bool = False) -> list['AbsPath']:
    """ Create abspath from polylines coords scaled for factor """
    coords = [pl.points[:] for pl in polylines]
    scaled_coords = [transform_points(co, factor, ROUNDING) for co in coords]
    if closed and len(coords) > 0:
        ## Join coords into a single path to allow island detection
        joined_coords = join_coords(scaled_coords)
        coords_string = ' '.join([get_path_coords(coords) \
                for coords in joined_coords])
        return [AbsPath(coords_string = coords_string, 
            coords_values = joined_coords)]
    return [AbsPath(coords_string = get_path_coords(coords), 
        coords_values = [coords]) for coords in scaled_coords]

