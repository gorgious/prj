#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
from prj.utils import get_resolution, get_render_basepath, UNITS, UNITS_SYSTEM
from prj.utils import debug_here

the_working_scene = None

def get_working_scene(**kwargs) -> 'Working_scene':
    global the_working_scene
    if the_working_scene:
        return the_working_scene
    working_scene = Working_scene(**kwargs)
    the_working_scene = working_scene
    return the_working_scene

def get_size_in_units(size: float, unit_system: str) -> str: 
    return str(size * UNITS_SYSTEM[unit_system]['svg_factor']) + \
            UNITS_SYSTEM[unit_system]['svg_unit']

## TODO Rename in Drawing_scene
class Working_scene:
    scene: bpy.types.Scene
    subjects: list['Drawing_subject']

    def __init__(self, resolution: list[int] = None, 
            raw_resolution: list[float] = None, 
            resolution_percentage: float = 1.0, scene_name: str='prj', 
            camera: bpy.types.Object= None):
        unit_system = bpy.context.scene.unit_settings.system
        units = bpy.context.scene.unit_settings.length_unit
        drawing_unit = 'METERS' if units == 'ADAPTIVE' else units
        self.scene = bpy.data.scenes.new(name=scene_name)
        self.scene.unit_settings.system = unit_system
        self.scene.unit_settings.length_unit = drawing_unit
        #self.scene.render.threads_mode = 'FIXED'
        #self.scene.render.threads = 1
        self.render_basepath = get_render_basepath()
        self.scene.render.engine = 'BLENDER_WORKBENCH'
        self.raw_resolution = raw_resolution
        if not raw_resolution: 
            self.raw_resolution = resolution
        self.resolution = resolution
        self.resolution_percentage = resolution_percentage
        self.resolution_correction = self.raw_resolution[0]/self.resolution[0]
        self.scene.render.resolution_percentage = int(
                resolution_percentage * 100)
        self.scene.display.render_aa = 'OFF'
        self.scene.display.shading.light = 'FLAT'
        self.scene.display.shading.color_type = 'OBJECT'
        self.scene.display_settings.display_device = 'None'
        self.scene.view_settings.view_transform = 'Standard'
        self.scene.view_settings.look = 'None'
        ## TODO check look, exposure and gamma too
        self.scene.render.film_transparent = True
        self.scene.render.image_settings.file_format = 'TIFF'
        self.scene.render.image_settings.tiff_codec = 'NONE'
        self.scene.render.image_settings.color_mode = 'RGBA'
        self.scene.render.resolution_x = resolution[0]
        self.scene.render.resolution_y = resolution[1]
        if camera:
            self.link_object(camera)
            self.scene.camera = camera
        self.subjects = []
        self.svg_size = get_size_in_units(self.raw_resolution[0], unit_system), \
                get_size_in_units(self.raw_resolution[1], unit_system)

    def add_subject(self, subject: 'Drawing_subject') -> None:
        self.subjects.append(subject)

    def link_object(self, obj: bpy.types.Object) -> None:
        self.scene.collection.objects.link(obj)

    def unlink_object(self, obj: bpy.types.Object) -> None:
        if obj.name in self.scene.collection.objects:
            self.scene.collection.objects.unlink(obj)

    def get_resolution_percentage(self) -> int:
        return self.scene.render.resolution_percentage

    def set_resolution_percentage(self, percentage: int) -> None:
        self.scene.render.resolution_percentage = int(percentage)

    def get_resolution(self) -> list[int]:
        return [self.scene.render.resolution_x, self.scene.render.resolution_y]

    def set_resolution(self, camera: bpy.types.Camera = None,
            drawing_scale: float = None, 
            resolution: list[int] = None) -> list[int]:
        if not resolution:
            resolution = get_resolution(camera, drawing_scale)
        self.scene.render.resolution_x = resolution[0]
        self.scene.render.resolution_y = resolution[1]
        return resolution
        
    def remove(self, del_subjs: bool = False, clear_objs: bool = False) \
            -> None:
        """ Unlink every objects in scene, delete them if clear is true
            and remove the scene. Remove the subject if del_subjs"""
        if del_subjs:
            for subj in self.subjects:
                subj.remove()
        all_objects = self.scene.collection.all_objects[:]
        for obj in all_objects:
            self.scene.collection.objects.unlink(obj)
            if clear_objs:
                bpy.data.meshes.remove(obj.data)
        bpy.data.scenes.remove(self.scene)
        global the_working_scene
        if self == the_working_scene:
            the_working_scene = None
