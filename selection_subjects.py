#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
from prj.working_scene import get_working_scene
from prj.utils import get_render_data

def get_subjects_from_previous_pixels(base_subjects: list['Drawing_subject'], 
        selected_subjects: list['Drawing_subject'], 
        render_pixels: 'ImagingCore' = None) -> list['Drawing_subject']:
    """ Get subjects from render_pixels previously occupied by 
        selected_subjects """
    previous_pixels_subjects = []
    for subject in selected_subjects:
        if not subject.previous_render_pixels:
            continue
        if subject.previous_render_pixels == subject.render_pixels:
            continue
        viewed_colors = set([render_pixels[pixel] \
                for pixel in subject.previous_render_pixels])
        subjects_from_prev = [subj for subj in base_subjects \
                if subj != subject and subj.color in viewed_colors]
        previous_pixels_subjects += [subj for subj in subjects_from_prev \
                if subj not in selected_subjects]
    return list(set(previous_pixels_subjects))

def get_selected_subjects(subjects: list['Drawing_subject'],
        selected_objects: list[bpy.types.Object]) -> list ['Drawing_subject']:
    """ Filter subjects based on selected_objects """

    if not selected_objects:
        return subjects

    selected_subjects = []
    for subj in subjects:
        if subj.is_symbol:
            ## TODO check if needed
            selected_subjects.append(subj)
            continue
        if subj.obj_eval.name \
                and subj.obj_eval.original in selected_objects:
            selected_subjects.append(subj)
            continue
        if subj.instancer_eval \
                and subj.instancer_eval.original in selected_objects:
            selected_subjects.append(subj)

    ## Get subjects from previous position
    base_res_render_data = get_render_data([], get_working_scene().scene)
    previous_pixels_subjects = get_subjects_from_previous_pixels(subjects,
            selected_subjects, base_res_render_data)
    ## TODO overlapping_subjects are those which are not changed because in front
    ##      of selected_subjects -> try to remove unchanged objects from selection
    overlapping_subjects = [over_subj for subj in selected_subjects 
            for over_subj in subj.overlapping_subjects \
                    if over_subj not in selected_subjects]

    return list(set(selected_subjects + previous_pixels_subjects + \
            overlapping_subjects))

